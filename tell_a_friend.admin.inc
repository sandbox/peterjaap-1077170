<?php
// $Id: tell_a_friend.admin.inc,v 1.0 2011/02/06 18:52:41 peterjaap Exp $

/**
 * @file
 * Administrative page callbacks for the tell_a_friend module.
 */

/**
 * General configuration form for controlling the tell_a_friend behaviour.
 */
function tell_a_friend_admin_settings() {
  $form['tell_a_friend_text'] = array(
    '#type' => 'fieldset',
    '#title' => t('E-mail content'),
	'#collapsible' => true,
  );

  $form['tell_a_friend_text']['tell_a_friend_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('tell_a_friend_subject', t('Subject')),
    '#description' => t('The default subject that is inserted into the Tell a Friend form.'),
  );
  
  $form['tell_a_friend_text']['tell_a_friend_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
	'#rows' => 10,
    '#default_value' => variable_get('tell_a_friend_message', "Hi! 
I just found this great website, I'm sure you'll appreciate it as well.

The URL is ".$GLOBALS['base_url']."

Cheers!"),
    '#description' => t('The default message that is inserted into the Tell a Friend form.'),
  );
  
  $form['tell_a_friend_extra'] = array(
    '#type' => 'fieldset',
    '#title' => t('Extra settings'),
	'#collapsible' => true,
	'#collapsed' => true
  );
  
  $form['tell_a_friend_extra']['tell_a_friend_show_firstscreen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show first screen'),
    '#default_value' => variable_get('tell_a_friend_show_firstscreen', 1),
    '#description' => t('Decide whether to show the first screen with input fields per email address.'),
  );
  
  $form['tell_a_friend_extra']['tell_a_friend_use_captcha'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use CAPTCHA'),
    '#default_value' => variable_get('tell_a_friend_use_captcha', 1),
    '#description' => t('Decide whether to use a captcha in the form. Highly recommended.'),
  );
  
  $form['tell_a_friend_extra']['tell_a_friend_default_send_copy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Default value for Send copy to self'),
    '#default_value' => variable_get('tell_a_friend_default_send_copy', 1),
    '#description' => t('Choose the default for send a copy; checked or unchecked.'),
  );
    
  $form['tell_a_friend_extra']['tell_a_friend_use_colorbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Colorbox'),
    '#default_value' => variable_get('tell_a_friend_use_colorbox', 0),
    '#description' => t('If you have the Colorbox module installed, check this box.'),
  );
  
  $form['tell_a_friend_extra']['tell_a_friend_thankyou'] = array(
    '#type' => 'textfield',
    '#title' => t('Thank you message'),
    '#default_value' => variable_get('tell_a_friend_thankyou', 'Thank you!'),
    '#description' => t('The text the users will see after the emails have been sent.'),
  );  
  
  $form['tell_a_friend_extra']['tell_a_friend_email_footer'] = array(
    '#type' => 'textfield',
    '#title' => t('Email footer'),
    '#default_value' => variable_get('tell_a_friend_email_footer', 'This mail has been sent using the Tell A Friend form on '.variable_get('site_name','')),
    '#description' => t('Use this field to tell people the email has been sent from your site, or add a disclaimer.'),
  );
  
  $form['tell_a_friend_twitter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter'),
	'#collapsible' => true,
	'#collapsed' => true
  );
  
  $form['tell_a_friend_twitter']['tell_a_friend_show_twitter'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Twitter button'),
    '#default_value' => variable_get('tell_a_friend_show_twitter', 1),
    '#description' => t('Decide whether to show a button that allows users to Tweet the link.'),
  );
  
  $form['tell_a_friend_twitter']['tell_a_friend_text_to_tweet'] = array(
    '#type' => 'textfield',
    '#title' => t('Text to Tweet'),
    '#default_value' => variable_get('tell_a_friend_text_to_tweet', "Hey I just found this great website; ".$GLOBALS['base_url']),
    '#description' => t('The text users will be able to tweet when Show Twitter button is checked. Max 140 characters.'),
  );

  return system_settings_form($form);
}