<?
// $Id: tell_a_friend.page.inc,v 1.0 2011/02/06 18:52:41 peterjaap Exp $
global $language;
$sitename = variable_get('site_name');
$theme_key = variable_get('theme_default','garland'); 
$settings = theme_get_settings($theme_key);
$modulepath = drupal_get_path('module', 'tell_a_friend');
drupal_add_css($modulepath."/tell_a_friend.base.css",'module','all',true);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl" lang="nl" dir="ltr">
<head>
	<title><?=$sitename;?> - Tell a Friend</title>
	<?php print drupal_get_css(); ?>
	<?php print drupal_get_js(); ?>
</head>

<body>
<div id="tell-a-friend-box" class="tell_a_friend">
	<img src="<?=$settings['logo_path'];?>" alt="<?=$sitename;?>" id="logo" />
	<h1><?=t('Send this to your friends');?></h1> 
	<? if(!isset($_POST['step']) AND variable_get('tell_a_friend_show_firstscreen',1)==1) { ?>
	<p><?=t('Fill out your friends\' email addresses:');?></p>
	<form action="tellafriend" method="post">
		<input type="hidden" name="step" value="1" />
		<ol>
			<? for($i=1;$i<=5;$i++) { ?>
				<li><label for=""><?=t("E-mail address")." ".$i;?>:</label> <input type="text" value="" name="email[]" /></li>
			<? } ?>
		</ol>
		<div class="navigation">
			<input type="submit" class="submit next" value="<?=t('Next');?>" />
		</div>
	</form>
	<? } elseif((isset($_POST['step']) AND $_POST['step']==1) OR variable_get('tell_a_friend_show_firstscreen',1)==0) { ?>
	<p><?=t('Send your friends the following message:');?></p>
	<form action="tellafriend" method="post">
		<input type="hidden" name="step" value="2" />
		<p class="textarea"><label><?=t('Friends');?></label><textarea name="emails" onclick="$(this).html('')"><? if(isset($_POST['email']) AND !empty($_POST['email'])) { foreach($_POST['email'] as $email) { echo $email."\n"; } }?></textarea></p>
		<p><label><?=t('E-mail address');?></label> <input type="text" name="email" value="<?=t('Your e-mail address');?>" onclick="jQuery(this).val('')" /></p>
		<p><label><?=t('Subject');?></label> <input type="text" name="subject" value="<?=variable_get('tell_a_friend_subject',t('Subject'));?>" onclick="jQuery(this).val('')" /></p>
		<p class="textarea message"><label><?=t('Message');?></label><textarea name="message"><?=variable_get('tell_a_friend_message',"Hi! I just found this great website, I'm sure you'll appreciate it as well. The URL is ".$GLOBALS['base_url'].". Cheers!");?></textarea></p>
		<p class="checkbox"><input type="hidden" name="copy" value="0" /><input type="checkbox" name="copy" value="1" /> <?=t('Send me a copy of this message.');?></p>
		<div class="navigation">
			<p class="previous"><a href="javascript:history.go(-1)">&laquo; <?=t('Previous'); ?></a></p>
			<p><input type="submit" class="submit next" value="<?=t('Next');?>" /></p>
		</div>
	</form>
	<? } elseif(isset($_POST['step']) AND $_POST['step']==2 AND variable_get('tell_a_friend_use_captcha',1)==1) { ?>
	<? $emails = trim($_POST['emails']); ?>
	<? if(empty($emails)) { ?>
		<?=t('You haven\'t entered any email addresses.');?> <a href="javascript:history.go(-1)"><?=t('Click here to go back.');?></a>
			<div class="navigation">
				<p class="previous"><a href="javascript:history.go(-1)">&laquo; <?=t('Previous');?></a></p>
			</div>
	<? } else { ?>
	<? $_SESSION['tell_a_friend_data'] = base64_encode(json_encode($_POST)); ?>
	<p><?=t('For security reasons we ask you to type over this code in the box below:');?></p>
	<form action="tellafriend" method="post">
	<input type="hidden" name="step" value="3" />
		<p class="verification"><label><?=t('Verification code');?></label> <img src="<?=$modulepath;?>/includes/captcha.php" alt="CAPTCHA" /> </p>
		<p>&nbsp;</p>
		<p class="verify"><label><?=t('Type over verification code');?> </label> <input type="text" value="" name="captcha" class="captcha" /></p>
		
		<div class="navigation">
			<p class="previous"><a href="javascript:history.go(-1)">&laquo; <?=t('Previous');?></a></p>
			<p><input type="submit" class="submit next" value="<?=t('Next');?>" /></p>
		</div>
	</form>
	<? } ?>
	<? } elseif((isset($_POST['step']) AND $_POST['step']==3) OR variable_get('tell_a_friend_use_captcha',1)==0) { ?>
		<? if(md5(strtolower($_POST['captcha']))==$_SESSION['encoded_captcha'] OR variable_get('tell_a_friend_use_captcha',1)==0) { ?>
		<?
			$data = json_decode(base64_decode($_SESSION['tell_a_friend_data']));
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: '.$data->email.'(via '.$sitename.' <'.variable_get("site_mail", ini_get("sendmail_from")).'>' . "\r\n";
			$headers .= 'Reply-To: '.$data->email.' <'.$data->email.'>' . "\r\n";
			
		?>
				<p><?=variable_get('tell_a_friend_thankyou','');?></p>
				<p><?=t('An e-mail has been sent to the following addresses');?></p>
				<ul>
					<?

					$emails = explode("\n",$data->emails);
					$data->message .= "<br /><br />
					==============================================================================================
					<em>".variable_get('email_footer','')."</em>
					==============================================================================================";
					if(!empty($emails) AND valid_email_address($email) AND valid_email_address($data->email)) {
						foreach($emails as $email) {
							echo "<li>".$email."</li>";
							mail($email,$data->subject,nl2br($data->message),$headers);
						}
					}
					if($data->copy==1 AND valid_email_address($data->email)) {
						mail($data->email,$data->subject,"<strong>".t("This is a copy of the mail sent from")." ".$sitename.".</strong><br /><br />".nl2br($data->message),$headers);
					}
					?>
				</ul>
				<?if(variable_get('tell_a_friend_show_twitter','default')==1) {?>
					<a target="_new" class="clean" href="http://twitter.com/?status=<?=rawurlencode(variable_get('tell_a_friend_text_to_tweet','default'));?>"><img src="<?=$modulepath;?>/includes/twitter_<?=$language->language;?>.png" /></a>
				<? } ?>
				<?if(variable_get('tell_a_friend_use_colorbox','default')==1) {?>
				<div class="navigation">
					<p><input type="submit" class="submit next" value="<?=t('Close');?>" onclick="parent.$.fn.colorbox.close()" /></p>
				</div>
				<? } ?>
		<? } else { ?>
			<?=t('The given verification code is incorrect.');?> <a href="javascript:history.go(-1)"><?=t('Click here to go back.');?></a>
				<div class="navigation">
					<p class="previous"><a href="javascript:history.go(-1)">&laquo; <?=t('Previous');?></a></p>
				</div>
		<? } ?>
	<? } ?>
</div>
</body>
</html>